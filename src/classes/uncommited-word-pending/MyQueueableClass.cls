//2. Create a class which asynchronously executes a callout.
public class MyQueueableClass implements Queueable, Database.AllowsCallouts {
    private void executeCallout() {
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint('http://example.com/');
        HTTPResponse res = new Http().send(req);
    }
    public void execute(QueueableContext ctx) {
        executeCallout();
    }}
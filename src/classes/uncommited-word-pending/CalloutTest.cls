//3. Create a class for the callout test.
@isTest
public class CalloutTest {
    public class CalloutServiceMock implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse response = new Httpresponse();
            response.setBody('OK');
            response.setStatusCode(200);
            return response;
        }
    }

    @isTest
    static void doTest() {
        MyFutureClass.asynchronousDml();
        Test.setMock(HttpCalloutMock.class, new CalloutServiceMock());
        Test.startTest();
        System.enqueueJob(new MyQueueableClass());
        Test.stopTest();
    }
}
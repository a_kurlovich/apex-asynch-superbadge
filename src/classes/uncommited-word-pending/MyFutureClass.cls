/**
 * Task. How to avoid uncommited word pending
 * https://trailblazer.salesforce.com/issues_view?id=a1p3A0000003UWIQA2
*/

public class MyFutureClass {
    @Future
    public static void asynchronousDml() {
        insert new Account(name = 'SFDC');
    }
}



@IsTest
private class AccountManagerTest {
    @IsTest
    static void testGetAccount() {

        Id recordId = createTestAccountRecord();
        RestRequest request = new RestRequest();

        request.requestURI = 'https://yourInstance.salesforce.com/services/Accounts/' + recordId + '/contacts';
        request.httpMethod = 'GET';
        RestContext.request = request;

        Account acc = AccountManager.getAccount();

        System.assert(acc != null);
        System.assertEquals('NameAccount', acc.Name);
       // System.assertEquals(acc.Contacts[0].LastName, 'LastNameContact');

    }


//helper
    static Id createTestAccountRecord() {

        Account account = new Account(
                Name = 'NameAccount'
        );
        insert account;

        Contact contact = new Contact(
                LastName = 'LastNameContact',
                AccountId = account.Id
        );
        insert  contact;
        return account.Id;
    }
}
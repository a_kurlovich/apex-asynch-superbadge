/**
 * Trailhead
 * https://trailhead.salesforce.com/en/content/learn/modules/apex_integration_services/apex_integration_webservices
 *
 * Create an Apex REST class that is accessible at /Accounts/<Account_ID>/contacts.
 * The service will return the account's ID and Name plus the ID and Name of all contacts associated
 * with the account. Write unit tests that achieve 100% code coverage for the class and run your Apex tests.
 *
 * UTEST AccountManagerTest
 *
 * Create an Apex class
 * Name: AccountManager
 * Class must have a method called getAccount
 * Method must be annotated with @HttpGet and return an Account object
 * Method must return the ID and Name for the requested record and all associated contacts with their ID and Name
 * Create unit tests
 * Unit tests must be in a separate Apex class called AccountManagerTest
 * Unit tests must cover all lines of code included in the AccountManager class, resulting in 100% code coverage
 * Run your test class at least once (via Run All tests the Developer Console) before attempting to verify this challenge
 *
*/

@RestResource(UrlMapping = '/Accounts/*/contacts')
global with sharing class AccountManager {

    @HttpGet
    global static Account getAccount() {

        RestRequest request = RestContext.request;

        // grab the AccountId from the end of the URL
        String accountId = request.requestURI.substringBetween('Accounts/', '/contacts');

        Account  account = [
                SELECT Id, Name, (SELECT id, LastName FROM Contacts)
                FROM Account
                WHERE Id = :accountId
        ];

        return account;

    }
}
public without sharing class UserUtils 
{
	public static final Map<String, Decimal> performanceTiers = new Map<String, Decimal>
	{
		'Low' => 0,
		'Medium' => 50000,
		'High' => 100000
	};

	@future
	public static void updateAnnualRevenue(Map<Id, Decimal> userIDsToAnnualRevenue)
	{
		// Retrieve users based on key values of the map: userIDsToAnnualRevenue
		List<User> users = getUsers(userIDsToAnnualRevenue.keySet());

		// Iterate through Users to determine if the performance tier
		// should be updated
		for (User user : users)
		{
			// Update Annual Revenue
			user.Annual_Revenue__c = userIDsToAnnualRevenue.get(user.Id);
			System.debug('user.Annual_Revenue__c: ' + user.Annual_Revenue__c);

			if (user.Annual_Revenue__c < performanceTiers.get('Medium'))
				user.Performance_Tier__c = 'Low';

			if (user.Annual_Revenue__c >= performanceTiers.get('Medium')
				&& user.Annual_Revenue__c < performanceTiers.get('High'))
				user.Performance_Tier__c = 'Medium';

			if (user.Annual_Revenue__c >= performanceTiers.get('High'))
				user.Performance_Tier__c = 'High';

			System.debug('user.Performance_Tier__c: ' + user.Performance_Tier__c);
		}

		update users;
	}

	public static List<User> getUsers(Set<Id> userIds)
	{
		List<User> users = [SELECT Id, Name, 
								Annual_Revenue__c,
								Performance_Tier__c
							FROM User
							WHERE Id IN :userIds];
		System.debug('users.size(): ' + users.size());

		return users;
	}

}
@isTest
private class AccountTest
{
	@testsetup
	static void setupData()
	{
		// Create multiple sales person users
		List<User> salesUsersToInsert = new List<User>();

		Profile adminProfile = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];

		// Low, medium, and high tier sales users
		User lowTierUser = new User(
			Performance_Tier__c = 'Low',
			FirstName = 'Low',
			LastName = 'User',
			Username = 'lowtieruser@example.com',
			Email = 'lowtieruser@example.com',
			CompanyName = 'Globomantics',
			Alias = 'alias',
			Title = 'Title',
			TimeZoneSidKey = 'America/Chicago',
			EmailEncodingKey = 'UTF-8',
			LanguageLocaleKey = 'en_US',
			LocaleSidKey = 'en_US',
			ProfileId = adminProfile.Id);

		User mediumTierUser = new User(
			Performance_Tier__c = 'Medium',
			FirstName = 'Medium',
			LastName = 'User',
			Username = 'mediumtieruser@example.com',
			Email = 'mediumtieruser@example.com',
			CompanyName = 'Globomantics',
			Alias = 'alias',
			Title = 'Title',
			TimeZoneSidKey = 'America/Chicago',
			EmailEncodingKey = 'UTF-8',
			LanguageLocaleKey = 'en_US',
			LocaleSidKey = 'en_US',
			ProfileId = adminProfile.Id);

		User highTierUser = new User(
			Performance_Tier__c = 'High',
			FirstName = 'High',
			LastName = 'User',
			Username = 'hightieruser@example.com',
			Email = 'hightieruser@example.com',
			CompanyName = 'Globomantics',
			Alias = 'alias',
			Title = 'Title',
			TimeZoneSidKey = 'America/Chicago',
			EmailEncodingKey = 'UTF-8',
			LanguageLocaleKey = 'en_US',
			LocaleSidKey = 'en_US',
			ProfileId = adminProfile.Id);

		// Add user records to list of users to insert
		salesUsersToInsert.add(lowTierUser);
		salesUsersToInsert.add(mediumTierUser);
		salesUsersToInsert.add(highTierUser);

		insert salesUsersToInsert;

		System.debug('lowTierUser.Id: ' + lowTierUser.Id + 
			'\nmediumTierUser.Id: ' + mediumTierUser.Id + 
			'\nhighTierUser.Id: ' + highTierUser.Id);


		// Create test Accounts
		List<Account> accountsToInsert = new List<Account>();
		Account lowAccount = new Account(Name = 'Low Account', AnnualRevenue = 20000, OwnerId = lowTierUser.Id);
		Account mediumAccount = new Account(Name = 'Medium Account', AnnualRevenue = 75000, OwnerId = mediumTierUser.Id);
		Account highAccount = new Account(Name = 'High Account', AnnualRevenue = 100000, OwnerId = highTierUser.Id);

		// Add accounts to list to insert
		accountsToInsert.add(lowAccount);
		accountsToInsert.add(mediumAccount);
		accountsToInsert.add(highAccount);

		insert accountsToInsert;
	}

	@isTest
	static void sortOwnersSuccess()
	{
		// Given:

		// Retrieve Users
		List<User> users = [SELECT Id, Performance_Tier__c FROM User WHERE LastName = 'User'];
		System.debug('users.size(): ' + users.size());

		// Retrieve Accounts
		List<Account> accounts = [SELECT Id, Name, OwnerId, AnnualRevenue FROM Account];
		System.debug('accounts.size(): ' + accounts.size());

		// When:
		List<Account> accountsToUpdate = new List<Account>();
		Test.startTest();

			// Alter annual revenue on an account
			System.debug('accounts[0] revenue before update: ' + accounts[0].AnnualRevenue);
			Decimal updatedRevenue = UserUtils.performanceTiers.get('High') + 50000;
			Account accountUpdate = new Account(Id = accounts[0].Id, AnnualRevenue = updatedRevenue);
			System.debug('accountUpdate.AnnualRevenue: ' + accountUpdate.AnnualRevenue);
			accountsToUpdate.add(accountUpdate);
			update accountsToUpdate;

		Test.stopTest();


		// Then:
		List<User> usersAfterUpdate = [SELECT Id, Performance_Tier__c 
									FROM User 
									WHERE LastName = 'User'];
		System.debug('usersAfterUpdate.size(): ' + usersAfterUpdate.size());

		Integer numberOfHighTierUsers = 0;
		for (User user : usersAfterUpdate)
			if (user.Performance_Tier__c == 'High')
				numberOfHighTierUsers += 1;

		System.assertEquals(2, numberOfHighTierUsers, '2 High Tier Users Found After Update');
	}
}
public without sharing class AccountUtils
{
	public static void sortOwners(List<Account> oldAccounts, List<Account> newAccounts)
	{
		// Map Accounts to their Users
		Map<Id, List<Account>> userIdToAccounts = getUserToAccounts(newAccounts);

		// For each user, calculate Account Revenue and decide if
		// performance tiers should be updated
		Map<Id, Decimal> userIdToRevenue = getUserRevenue(userIdToAccounts);

		// Calls the future method to make Annual Revenue updates on the user
		if (userIdToAccounts.keySet().size() > 0)
			UserUtils.updateAnnualRevenue(userIdToRevenue);
	}

	public static Map<Id, List<Account>> getUserToAccounts(List<Account> accountsByUsers)
	{
		Map<Id, List<Account>> userIdToAccounts = new Map<Id, List<Account>>();
		for(Account account : accountsByUsers)
			if (account.AnnualRevenue != null && account.AnnualRevenue != 0)
				if (userIdToAccounts.containsKey(account.OwnerId))
						userIdToAccounts.get(account.OwnerId).add(account);
				else
					userIdToAccounts.put(account.OwnerId, new List<Account>{account});
		System.debug('userIdToAccounts.keySet().size(): ' + userIdToAccounts.keySet().size());

		return userIdToAccounts;
	}

	public static Map<Id, Decimal> getUserRevenue(Map<Id, List<Account>> userIdToAccounts)
	{
		Map<Id, Decimal> userIdToRevenue = new Map<Id, Decimal>();
		for (Id userId : userIdToAccounts.keySet())
		{
			// Iterate through Accounts to retrieve total
			Decimal totalRevenue = 0;
			for (Account account : userIdToAccounts.get(userId))
				totalRevenue += account.AnnualRevenue;

			userIdToRevenue.put(userId, totalRevenue);
		}

		return userIdToRevenue;
	}
}
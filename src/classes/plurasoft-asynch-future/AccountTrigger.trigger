trigger AccountTrigger on Account (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) 
{
	if (Trigger.isBefore) 
		if (Trigger.isUpdate)
			// Sort the owners based on total revenue of Accounts
			AccountUtils.sortOwners(Trigger.old,Trigger.new);
	else if (Trigger.isAfter) 
	{
		// After events may go here later
	}
}
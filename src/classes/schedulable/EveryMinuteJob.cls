/**
 * Calls Chron every N minutes
 *
 * https://www.avenga.com/magazine/scheduled-jobs-in-salesforce/
*/

public class EveryMinuteJob implements Schedulable {

    private Integer delayInMinutes;
    private CronTrigger mainJob;

    // Constructor of Main job
    public EveryMinuteJob(Integer delayInMinutes) {
        this.delayInMinutes = delayInMinutes;
    }

    // Constructor of worker job
    private EveryMinuteJob(Integer delayInMinutes, CronTrigger parentJob) {
        this(delayInMinutes);
        mainJob = parentJob;
    }

    public void execute(SchedulableContext sc) {
        Id jobId = sc.getTriggerId();
        if (mainJob == null || mainJob.Id == jobId) {
            mainJob = [
                    SELECT CronJobDetail.Name, NextFireTime
                    FROM CronTrigger WHERE Id = :jobId
            ];
        } else {
            System.abortJob(jobId);
        }

        Datetime nextWorkerRun = Datetime.now().addMinutes(delayInMinutes);
        if (nextWorkerRun.addMinutes(delayInMinutes) < mainJob.NextFireTime) {
            String cronExp = nextWorkerRun.second() + ' '
                    + nextWorkerRun.minute() + ' '
                    + nextWorkerRun.hour() + ' '
                    + nextWorkerRun.day() + ' '
                    + nextWorkerRun.month()
                    + ' ? '
                    + nextWorkerRun.year();
            System.schedule(
                    mainJob.CronJobDetail.Name + ' (worker)',
                    cronExp,
                    new EveryMinuteJob(delayInMinutes, mainJob)
            );
        }

        try {
            // run action here
        } catch(Exception ex) {
            // log errors here
        }
    }
}
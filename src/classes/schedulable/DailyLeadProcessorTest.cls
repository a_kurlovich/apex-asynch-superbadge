@IsTest
private class DailyLeadProcessorTest {
    // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    private static final String NEW_LEAD_SOURCE = 'Dreamforce';

    @TestSetup
    static void setup() {
        List<Lead> leads = new List<Lead>();

        for(Integer i = 0; i < 200; i ++) {
            leads.add(new Lead(LastName = 'LName ' + i, Company = 'CompanyName ' + i));

        }
        insert  leads;
    }

    @IsTest
    static void test_DailyLeadProcessor() {
        DailyLeadProcessor  dailyLead = new DailyLeadProcessor();

        // Within startTest and stopTest Update is not executed yet
        Test.startTest();
        String jobId = System.schedule('Update empty LeadSource', CRON_EXP, dailyLead);

        List<Lead> leadBeforeUpdate = [SELECT id FROM Lead WHERE LeadSource = :NEW_LEAD_SOURCE ];

        System.assertEquals(0, leadBeforeUpdate.size());
        Test.stopTest();

        // After schedule executed, fields have updated
        List<Lead> leadAfterUpdate = [SELECT id FROM Lead WHERE LeadSource = :NEW_LEAD_SOURCE ];

        System.assertEquals(200, leadAfterUpdate.size());
    }
}
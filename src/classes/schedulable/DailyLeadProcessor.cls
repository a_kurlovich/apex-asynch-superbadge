/**
 * Trailhead
 * Apex Scheduler
 * https://trailhead.salesforce.com/content/learn/modules/asynchronous_apex/async_apex_scheduled
 *
 * Create an Apex class that implements the Schedulable interface to update Lead records with
 * a specific LeadSource. (This is very similar to what you did for Batch Apex.)
 *
 * UTEST: DailyLeadProcessorTest
 */

global class DailyLeadProcessor implements Schedulable {

    private final String NEW_LEAD_SOURCE = 'Dreamforce';

    global void execute(SchedulableContext ctx) {
        List<Lead> leadsSourceEmpty = [SELECT id, LeadSource from Lead WHERE LeadSource = ''];

        if (leadsSourceEmpty.size() > 0) {
            List<Lead> leadsSourceUpdated = new List<Lead>();

            for (Lead lead : leadsSourceEmpty) {
                lead.LeadSource = NEW_LEAD_SOURCE;
                leadsSourceUpdated.add(lead);
            }
            update leadsSourceUpdated;
        }
    }
}
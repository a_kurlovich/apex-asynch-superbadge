/**
 * Created by alex on 4/30/2021.
 */

@IsTest
global  class ContactCalloutMock implements HttpCalloutMock{

    global  HttpResponse respond(HttpRequest req) {
        HttpResponse res = new HttpResponse();

        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"status":"success"}');
        res.setStatusCode(200);
        return res;

    }
}
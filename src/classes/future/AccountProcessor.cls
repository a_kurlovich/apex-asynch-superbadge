/**
 * Trailhead
 * https://trailhead.salesforce.com/content/learn/modules/asynchronous_apex/async_apex_future_methods
 *
 * Create an Apex class with a future method that accepts a List of Account IDs and updates a custom
  * field on the Account object with the number of contacts associated to the Account. Write unit tests
  * that achieve 100% code coverage for the class. Every hands-on challenge in this module asks you
  * to create a test class.
  *
  * Covers Unit Test: AccountProcessorTest, ContactCalloutMock
 */

public with sharing class AccountProcessor {

    @Future
    public static void countContacts(List<ID> accountIds) {

        Integer numContacts = 0;
        List<Account> accountList = [SELECT Id, Number_Of_Contacts__c, (SELECT Id FROM Contacts) FROM Account WHERE Id in: accountIds];

        for(Account myAccount: accountList) {
            numContacts = myAccount.Contacts.size();
            myAccount.Number_Of_Contacts__c = numContacts;

        }
             upsert  accountList;
    }
}
/** ===============================================================================================
 * Test class
 * Covers AccountProcessor
 *
 * ================================================================================================
 */

@IsTest
private class AccountProcessorTest {

    @TestSetup
    static void setup(){
        List<Account> accounts = new List<Account>{
                new Account(
                        Name = 'First Account'
                ),
                new Account(
                        Name = 'Second Account'
                )
        };
        insert accounts;

        List<Contact> contacts = new List<Contact> {
                new Contact(
                        LastName = 'First Contact LName',
                        AccountId = accounts[0].id
                ),
                new Contact(
                        LastName = 'First Contact LName',
                        AccountId  = accounts[0].id
                ),
                new Contact(
                        LastName = 'Third ContactLName',
                        AccountId = accounts[1].id
                )
        };

        insert contacts;


    }


    @IsTest
    private static void test_CountContacts() {
/*
        List<Account> accounts = [SELECT id FROM Account];

        List<Id> accountsIds = new List<Id>();

        for(Account account: accounts) {
            accountsIds.add(account.id) ;
        }

     */
        Map<id, Account> accountMap = new Map<id, Account> ([SELECT id, Contacts FROM Account]);

        Test.setMock(HttpCalloutMock.class, new ContactCalloutMock());

        Test.startTest();
        AccountProcessor.countContacts((List<Id>) accountMap.keySet());
        Test.stopTest();

        List<Account> accountList = [SELECT Id, Number_Of_Contacts__c FROM Account LIMIT 2];

       System.assertEquals(2, accountList[0].Number_Of_Contacts__c);
       System.assertEquals(1, accountList[1].Number_Of_Contacts__c);

    }
}
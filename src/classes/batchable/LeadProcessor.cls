/**
 * Thrainlead
 *
 * Create an Apex class that implements the Database.Batchable interface to update
 * all Lead records in the org with a specific LeadSource.
 *
 * UTest: LeadProcessorTest
 */
public with sharing class LeadProcessor Implements Database.Batchable<sObject>, Database.AllowsCallouts {
    private final String NEW_LEAD_SOURCE = 'Dreamforce';

    // 50 000 000 records selected!
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
                'SELECT ID FROM Lead'
        );
    }

    // Scope has what start() returns: SELECT ID FROM Lead, each batch for 200 records
    public void execute(Database.BatchableContext bc, List<Lead> scope) {
        // List to update  outside of FOR
        List<Lead> leads = new List<Lead>();
        for (Lead modifiedLead : scope) {
            modifiedLead.LeadSource = NEW_LEAD_SOURCE;
            leads.add(modifiedLead);
        }
        update leads;
    }

    public void finish(Database.BatchableContext param1) {
        System.debug('First batch finished, second started');

        UpdateContactAddresses uca = new UpdateContactAddresses();
        Database.executeBatch(uca);


    }
}
/**
 * UTEST
 *
 * Create an Apex test class:
 * Name: AddPrimaryContactTest
 * In the test class, insert 50 Account records for BillingState NY and 50 Account records for BillingState CA
 * Create an instance of the AddPrimaryContact class, enqueue the job, and assert that a Contact record was
 * inserted for each of the 50 Accounts with the BillingState of CA
 *
 */
@IsTest
private class  AddPrimaryContactTest {

    @TestSetup
    static void setup() {

        List<Account> accs = new List<Account>();

        for (Integer i = 0; i < 50; i++) {
            accs.add(new Account(Name = 'AccName ' + i++, BillingState = 'NY'));
            accs.add(new Account(Name = 'AccName ' + i++,  BillingState = 'CA'));

        }
        insert accs;

    }

    @IsTest
    static void test() {

        Contact con = new Contact(LastName = 'TestCont');
        AddPrimaryContact addPCIns = new AddPrimaryContact(CON ,'CA');

        Test.startTest();
        System.enqueueJob(addPCIns);
        Test.stopTest();

        System.assertEquals(50, [select count() from Contact]);
    }
}
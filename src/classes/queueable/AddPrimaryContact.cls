/**
 * Created by alex on 5/3/2021.
 */
/**
 * Trainhead
 * https://trailhead.salesforce.com/content/learn/modules/asynchronous_apex/async_apex_queueable
 *
 * Create a constructor for the class that accepts as its first argument a Contact sObject and a second
 * argument as a string for the State abbreviation The execute method must query for a maximum of 200
 * Accounts with the BillingState specified by the State abbreviation passed into the constructor and
 * insert the Contact sObject record associated to each Account. Look at the sObject clone() method.
 *
 * UTest: AddPrimaryContactTest
 */
public class AddPrimaryContact implements Queueable, Database.AllowsCallouts {

    private Contact contact;
    private String state;


    public AddPrimaryContact(Contact contact, String state) {
        this.contact = contact;
        this.state = state;
    }

    public void execute(QueueableContext context) {


        List<Account> accountByState = [SELECT Id, Name, (SELECT id, FirstName, LastName FROM  Contacts)
        FROM Account WHERE BillingState = :state LIMIT 200];
        List<Contact> contacts = new List<Contact>();

        for (Account acc : accountByState) {
            Contact cont = contact.clone(false, false, false, false);
            cont.AccountId = acc.id;
            contacts.add(cont);
        }

        if (contacts.size() > 0) {
            insert contacts;
        }

    }
}
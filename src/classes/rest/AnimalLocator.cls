/**
 * Trailhead
 * https://trailhead.salesforce.com/content/learn/modules/apex_integration_services/apex_integration_rest_callouts
 *
 * Create an Apex class that calls a REST endpoint to return the name of an animal, write unit tests
 * that achieve 100% code coverage for the class using a mock response, and run your Apex tests.
 *
 *
 * UTEST AnimalLocatorTest, AnimalLocatorMock
*/

public class AnimalLocator {
    private static final String END_POINT = 'https://th-apex-http-callout.herokuapp.com/animals/';

    private static Object objectResult;
    private static String name;

    public static String getAnimalNameById(Integer id) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(END_POINT + id);
        request.setMethod('GET');
        HttpResponse response = http.send(request);

        if (response.getStatusCode() == 200) {
            // Deserializes the JSON string into collections of primitive data types.
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            System.debug(results);
            // 04:54:18.25 (219717039)|USER_DEBUG|[27]|DEBUG|{animal={eats=berries, campers, adam
            // seligman, id=2, name=bear, says=yum yum}}

            Map<String, Object> animals = (Map<String, Object>) results.get('animal');
            System.debug(animals);
            // 04:54:18.25 (219791690)|USER_DEBUG|[31]|DEBUG|{eats=berries, campers, adam seligman,
            // id=2, name=bear, says=yum yum}

            name = string.valueOf(animals.get('name'));
            System.debug(name);
            // 04:54:18.25 (219840513)|USER_DEBUG|[34]|DEBUG|bear
        }
        return name;
    }
}
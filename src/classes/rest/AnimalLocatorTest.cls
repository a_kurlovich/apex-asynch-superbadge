@IsTest
private class AnimalLocatorTest {

    @IsTest
    static void testGetAnimalNameById() {
        String expectedName = 'chicken';

        Test.setMock(HttpCalloutMock.class, new AnimalLocatorMock());

        String result = AnimalLocator.getAnimalNameById(1);
        System.assertEquals(expectedName, result);

    }
}
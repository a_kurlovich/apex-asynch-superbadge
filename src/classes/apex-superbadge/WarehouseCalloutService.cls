/**
 * Created by alex on 5/10/2021.
 */

public class WarehouseCalloutService implements Queueable, Database.AllowsCallouts {

    public void execute(QueueableContext qc) {
        warehouseGetCallout();
    }

    public static void warehouseGetCallout() {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        request.setEndpoint(Parameters.REST_END_POINT);
        HttpResponse response = http.send(request);

        if (response.getStatusCode() == 200) {

            // results - List of all items JSON
            // EX: ({_id=55d66226726b611100aaf741, cost=5000, lifespan=120, maintenanceperiod=365,
            // name=Generator 1000 kW, quantity=5, replacement=false, sku=100003}, {_id=55d66226726b611100aaf742,
            // cost=300, life ...
            List<Object> results = (List<Object>) JSON.deserializeUntyped(response.getBody());
            System.debug('results: ' + results);

            List<Product2> equipments = new List<Product2>();

            for (Object item : results) {
                // mapJson - Map of ONE Item JSON, so we can have MAP (key, value)
                // EX: 03:30:47.45 (834771975)|USER_DEBUG|[27]|DEBUG|mapJson: {_id=55d66226726b611100aaf741, cost=5000,
                // lifespan=120, maintenanceperiod=365, name=Generator 1000 kW, quantity=5, replacement=false, sku=100003}
                Map<String, Object> mapJson = (Map<String, Object>) item;
                System.debug('mapJson: ' + mapJson);
                Product2 equipment = new Product2();
                equipment.Replacement_Part__c = (Boolean) mapJson.get('replacement');
                equipment.Name = (String) mapJson.get('name');
                equipment.Maintenance_Cycle__c = (Integer) mapJson.get('maintenanceperiod');
                equipment.Lifespan_Months__c = (Integer) mapJson.get('lifespan');
                equipment.Cost__c = (Decimal) mapJson.get('cost');
                equipment.Warehouse_SKU__c = (String) mapJson.get('sku');
                equipment.Current_Inventory__c = (Double) mapJson.get('quantity');
                equipments.add(equipment);
            }

            if (equipments.isEmpty()) {
                upsert equipments;
            }
        }
    }
}
public  class MaintenanceRequestHelper {
    public static void updateWorkOrders(List<Case> newList, Map<Id,Case> oldMap) {

        List<Id> caseUpdatedId = new List<Id>();

        // Validation if Trigger.OldMap record really changed to Status == 'Closed', otherwise
        // doesn't have sense to process
 for (Case item: newList) {
            if (oldMap.get(item.Id).Status != Parameters.CASE_STATUS_CLOSED &&
                    item.Status == Parameters.CASE_STATUS_CLOSED) {
                if (item.Type == Parameters.CASE_TYPE_REPAIR || item.Type == Parameters.CASE_TYPE_ROUTINE_MAINTENANCE) {
                    caseUpdatedId.add(item.Id);
                }
            }
        }

        if (!caseUpdatedId.isEmpty()) {

            // Select all Case records that changed
            List<Case> caseUpdatedData = new List<Case>([
                    SELECT ClosedDate, Date_Due__c, Date_Reported__c, Description,
                            EngineeringReqNumber__c, PotentialLiability__c, Product__c,
                            SLAViolation__c, Status, Subject, Type, Vehicle__c
                    FROM Case
                    WHERE Id IN :caseUpdatedId
            ]);

            // Since there is Join table 'Equipment_Maintenance_Item__c' between Case and Product2
            // we need Equipment__r.Maintenance_Cycle__c
            List<Equipment_Maintenance_Item__c> equipAtCaseList = new List<Equipment_Maintenance_Item__c>([
                    SELECT Equipment__c,  Maintenance_Request__c, Quantity__c, Equipment__r.Maintenance_Cycle__c
                    FROM Equipment_Maintenance_Item__c
                    WHERE Maintenance_Request__c IN :caseUpdatedId
            ]);

            // Gets minimum cycle to put in Case.Date_Due__c
            System.debug('case Id ' + caseUpdatedId);
            System.debug('equipAtCaseList ' + equipAtCaseList);

            Decimal minCycle = 0;
            if (!equipAtCaseList.isEmpty()) {
                minCycle = 999999;
                for (Equipment_Maintenance_Item__c item : equipAtCaseList) {
                    if (minCycle > item.Equipment__r.Maintenance_Cycle__c) {
                        minCycle = item.Equipment__r.Maintenance_Cycle__c;
                    }
                }
            }

            Date today = Date.today();
            List<Case> newCases = new List<Case>();
            for (Integer i = 0; i < caseUpdatedId.size(); i++) {
                Case item = new Case();
                item.ParentId = caseUpdatedId[i];
                item.Status = Parameters.CASE_STATUS_NEW;
                item.Subject = Parameters.CASE_SUBJECT_ROUTINE_MAINTENANCE_REQUEST + i;
                item.Type = Parameters.CASE_TYPE_ROUTINE_MAINTENANCE;
                item.EngineeringReqNumber__c = caseUpdatedData[i].EngineeringReqNumber__c;
                item.PotentialLiability__c = caseUpdatedData[i].PotentialLiability__c;
                item.Product__c = caseUpdatedData[i].Product__c;
                item.SLAViolation__c = caseUpdatedData[i].SLAViolation__c;
                item.Vehicle__c = caseUpdatedData[i].Vehicle__c;
                item.Date_Reported__c = today;
                item.Date_Due__c =  today.addDays((Integer) minCycle);

                newCases.add(item);
            }
            insert newCases;

            // Link previous Equipment t0 new Case
            List<Equipment_Maintenance_Item__c> updateEquipMaintenance = new List<Equipment_Maintenance_Item__c>();
            for (Case itemCase: newCases) {
                for (Equipment_Maintenance_Item__c itemEquipMaintItem: equipAtCaseList) {
                    if (itemEquipMaintItem.Maintenance_Request__c == itemCase.ParentId) {
                        itemEquipMaintItem.Maintenance_Request__c = itemCase.Id;
                        updateEquipMaintenance.add(itemEquipMaintItem);
                    }
                }
            }
            update updateEquipMaintenance;
        }
    }
}
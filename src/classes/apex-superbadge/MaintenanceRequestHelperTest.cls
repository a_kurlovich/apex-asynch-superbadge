/**
 * Created by alex on 5/10/2021.
 */
@IsTest
public class MaintenanceRequestHelperTest {
    public static final Integer CASES_NUM = 10;
    public static final Integer PRODUCTS_NUM = 10;

    @TestSetup
    public static void initializeClassTestData() {
        createTestData();
    }

    public static void createTestData() {

        // List<Case> insert
        List<Case> beforeUpdate = new List<Case>();
        for (Integer i = 0; i < CASES_NUM; i++) {
            beforeUpdate.add(new Case(
                    Status = Parameters.CASE_STATUS_NEW,
                    Type = Parameters.CASE_TYPE_REPAIR,
                    Subject = 'TestSubject' + i
                    ));
        }
       insert beforeUpdate;

        // List<Product2> insert
        List<Product2> equipmentsList = new List<Product2>();
        for (Integer i = 1; i <= PRODUCTS_NUM; i++) {
            equipmentsList.add(new Product2(Name = 'Product' + i, Maintenance_Cycle__c = i,
                    Replacement_Part__c = true));
        }
        insert equipmentsList;

        // List<Equipment_Maintenance_Item__c> insert with lookups Product2, Case
        List<Equipment_Maintenance_Item__c> equipmentMaintItemsList = new List<Equipment_Maintenance_Item__c>();
        for (Case caseItem : beforeUpdate) {
            for (Product2 equipmentItem : equipmentsList) {
                Equipment_Maintenance_Item__c equipmMaintItem = new Equipment_Maintenance_Item__c();
                equipmMaintItem.Maintenance_Request__c = caseItem.Id;
                equipmMaintItem.Equipment__c = equipmentItem.Id;
                equipmentMaintItemsList.add(equipmMaintItem);
            }
        }
        insert equipmentMaintItemsList;
    }


    @IsTest
    private static void testUpdateCase() {

        List <Case> caseList = new List <Case>([SELECT Id, Status, Type, Subject from Case]);
        System.debug('BEFORE: ' + caseList);
        Test.startTest();
        Integer i = 0;
        for (Case item : caseList) {
            item.Status = Parameters.CASE_STATUS_CLOSED;
            item.Subject = 'Subj' + i++;

        }
        update caseList;

        Test.stopTest();
        System.debug('AFTER' + caseList);
        List<Case> caseNumAfterUpdate = [Select id From Case];

        List<Equipment_Maintenance_Item__c> equipmentMaintItemsList = [
                SELECT id, Maintenance_Request__c, Equipment__c
                FROM Equipment_Maintenance_Item__c
                WHERE id = :caseNumAfterUpdate[0].id
        ];

        // Close Case creates new Case
        System.assertEquals(caseList.size() + caseList.size(), caseNumAfterUpdate.size());

    }
}
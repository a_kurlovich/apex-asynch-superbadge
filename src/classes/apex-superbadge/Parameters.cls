/**
 * Defines parameters
 *
 *
 */

public class Parameters {

    public static final String CASE_TYPE_REPAIR = 'Repair';
    public static final String CASE_TYPE_ROUTINE_MAINTENANCE = 'Routine Maintenance';
    public static final String CASE_SUBJECT_ROUTINE_MAINTENANCE_REQUEST = 'Routine Maintenance Request';
    public static final String CASE_STATUS_NEW = 'New';
    public static final String CASE_STATUS_CLOSED = 'Closed';

    public static final String REST_END_POINT = 'https://th-superbadge-apex.herokuapp.com/equipment/';

    public static final String SCHEDULE_CRON = '0 0 1 * * ? *';

}
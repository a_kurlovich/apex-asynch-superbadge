/**
 * Created by alex on 5/12/2021.
 */
@IsTest
public class WarehouseSyncScheduleTest {

    public static String CRON_EXP = '0 0 0 3 9 ? 2022';
    @IsTest
    static void testSchedule() {
        Test.setMock(HttpCalloutMock.class, new WarehouseCalloutServiceMock());

        Test.startTest();
        String job = System.schedule('TestJob', CRON_EXP, new WarehouseSyncSchedule());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :job];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        // Verify the expressions are the same System.assertEquals(CRON_EXP, ct.CronExpression);
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        // Verify the next time the job will run
        System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
        Test.stopTest();
    }
}
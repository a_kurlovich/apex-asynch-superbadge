/**
 * Created by alex on 5/12/2021.
 */

@IsTest
private class WarehouseCalloutServiceTest {
    @IsTest
    static void callout() {
        Test.setMock(HttpCalloutMock.class, new WarehouseCalloutServiceMock());
        WarehouseCalloutService.warehouseGetCallout();

        Test.startTest();
        System.enqueueJob(new WarehouseCalloutService());
        Test.stopTest();
        System.assertEquals(1,[SELECT count()  FROM Product2]);
    }
}


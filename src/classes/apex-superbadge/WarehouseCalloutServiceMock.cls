/**
 * Created by alex on 5/12/2021.
 */
@IsTest
global class WarehouseCalloutServiceMock implements HttpCalloutMock {

    global HttpResponse respond(HttpRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('[{"_id":"55d66226726b611100aaf751","replacement":true,"quantity":165,"name":"Motor","maintenanceperiod":0,"lifespan":0,"cost":150,"sku":"100019"},{"_id":"55d66226726b611100aaf752","replacement":true,"quantity":210,"name":"Breaker"}]');
        response.setStatusCode(200);
        return response;
    }
}
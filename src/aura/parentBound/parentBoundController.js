/**
 * Created by alex on 6/2/2021.
 */

/* childExprController.js */
({
    updateChildAttr: function(cmp) {
        cmp.set("v.childAttr", "updated child attribute");
    }
})
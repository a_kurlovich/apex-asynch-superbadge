({
    doInit : function(component, event, helper) {

        // Get the empApi component
        const empApi = component.find('empApi');
        // Get the channel from the input box
        const channel = '/event/Order_Event__e';
        // Replay option to get new events
        const replayId = -1;

        // Subscribe to an event
        empApi.subscribe(channel, replayId, $A.getCallback(eventReceived => {
            var casePayload = eventReceived.data.payload;

            // Process event
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": casePayload.CaseId__c
            });
            navEvt.fire();
        }))
            .then(subscription => {
                // Post Subscription Logic.
                console.log('Subscription request sent to: ', subscription.channel);
                // Save subscription to unsubscribe later
                component.set('v.subscription', subscription);
            });
    }
})
/**
 * Created by alex on 6/2/2021.
 */

({
    callAuraMethod : function(component, event, helper) {
        //Call Child aura method
        var childComponent = component.find("childCmp");
        var message = childComponent.childMessageMethod();
    }
})